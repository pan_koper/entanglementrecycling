# README #


### What is this repository for? ###

* Codes invoved in the study of entanglement recycling procedure (see [1](https://arxiv.org/abs/1209.2683), [2](https://arxiv.org/abs/2105.14886))

### Dependencies  ###

* Python3
* Sage (with Python3)

### Scripts

* `optimal_fidelity.py` - script allowing to calculate the recycling fidelity after one round of deterministic PBT. In fact it uses some legacy functions that are excessive for this task, e.g there is no need for the code that handles addition of `k` boxes to a given Young frame, but in this context it is used with `k=1` anyway.
* recycling_state_fidelity.py - legacy script, calculating the recycling fidelity in qubit case, based on the closed form expressions (i.e. not depending on arbitrary on the parameters of irreps of symmetric group)
* `plot.nb` - Mathematica script for plotting stuff
