import math
from sage.all import SemistandardTableaux, StandardTableaux, Partitions, matrix, QQ, vector, lognormvariate
from itertools import chain


import math as m
import scipy.special as ssp

# SN_IRREPS = Partitions(N, max_length = d).list()
# SNm1_IRREPS = Partitions(N-1, max_length = d).list()


def add_box(p, d):
    '''function that adds box to a Young frame p in such a way that the total height
    does not exceed d. The list of possible frames is returned.'''
    res = []

    for i,row in enumerate(p):    
        if (i > 0 and p[i-1] > row) or i == 0:
            _tmp = p[:]
            _tmp[i] = row + 1
            res.append(_tmp)
    if len(p) < d:
        _tmp = p[:]
        _tmp.append(1)
        res.append(_tmp)
    return res

def tableaux_distance(p1, p2):
    '''number of boxes in frame p1 different from frame p2. It is asumed
    that p1 and p1 are partitions of the same number, othervise -1 is returned.'''
    if sum(p1) != sum(p2):
        return -1
    return sum(p1) - common_boxes(p1, p2)

def common_boxes(p1, p2):
    '''number of the overlapping boxes in Young frames p1 and p2'''
    return sum([min(x,y) for x,y in zip(p1, p2)])

def add_multiple_boxes(p_table, d, k):
    '''generalization allowing addition of k boxes to frame p_table
    could do with refactoring '''
    new = [add_box(pp, d) for pp in p_table]
    if (k==1): return list(chain(*new))
    
    res = []
    for p in new:
        res.append(add_multiple_boxes(p, d, k-1))
    return list(chain(*res))

def gen_matrix(N, k, d):

    partitions = Partitions(N, max_length=d).list()
    partitions2 = Partitions(N-k, max_length=d).list()

    M = [[0 for i in partitions] for i in partitions]
    for i in partitions:

        for j in partitions:

            if  -1 < tableaux_distance(i, j) <= 1:

                entry = 0
                for a in partitions2:

                    entry += find_ways_from_alpha_to_mu(a, i, k, d) * find_ways_from_alpha_to_mu(a, j, k, d)

                M[partitions.index(i)][partitions.index(j)] = entry
    return M

def find_ways_from_alpha_to_mu(a, m, k, d):
    possible_diagrams=add_multiple_boxes([a], d, k)
    return possible_diagrams.count(m)
def power_iter(matr):
    '''power iteration determining maximal eigenvalue of
    teleportation matrix'''

    dim = matr.nrows()
    w = vector([lognormvariate(2,1) for i in range(dim)])
    w = w/sum(w)    
    v = w

    rho_m = 0    
    for x in range(0,10000):
        v = v*matr
        rho_m = math.sqrt(sum([x**2 for x in v]))
        v = v / rho_m
    return (rho_m, v)

def mul(alpha, d):
    '''multiplicity of irrep alpha in the natural representation of S(n) on n copies of C^d'''
    return SemistandardTableaux(alpha, max_entry=d).cardinality()

def dim(alpha):
    '''dimension of the irrep alpha'''
    return StandardTableaux(alpha).cardinality()

def dt(theta, d):
    '''dimension of 'forbidden' irrep theta obtained by adding one box, whiches height equals d+1'''
    if len(theta) == d:
        return StandardTableaux(list(theta) + [1]).cardinality()
    else:
        return 0

def sum_md_sq( alpha, d):
    '''sum of square roots of m_\alpha times d_\mu, for alpha inalid for arbitrary d'''
    res = 0
    for nu in add_box(alpha, d):
        res += m.sqrt(mul(nu, d)*dim(nu))
    return res

def F_gen(N, d):
    '''non optimal fidelity, valid for arbitrary d'''
    res = 0

    for alpha in Partitions(N-1, max_length=d-1).list():
        res += 1/N * sum_md_sq(alpha, d)**2 * m.sqrt(N) / d**(N+1)
    for alpha in Partitions(N-1, length=d).list():
        res += 1/m.sqrt(N * dim(alpha) - dt(alpha, d)) * m.sqrt(dim(alpha) / N) * sum_md_sq(alpha, d)**2 * m.sqrt(N) / d**(N+1)

    return(res)

def v(alpha, d):
    '''element labelled by irrep alpha, of the eigenvector corresponding to the
    maximal eigenvalue of teleportation matrix'''

    N = sum(alpha)
    m_N = gen_matrix(N, 1, d)
    v_N = power_iter(matrix(QQ, m_N))[1]
    SN_IRREPS = Partitions(N, max_length = d).list()

    return v_N[SN_IRREPS.index(alpha)]


def F_opt(N, d):
    '''optimal fidelity, valid for arbitrary d'''

    res = 0
    m_Nm1 = gen_matrix(N-1, 1, d)
    mt = matrix(QQ, m_Nm1)
    v_Nm1 = power_iter(mt)[1]
    
    m_N = gen_matrix(N, 1, d)
    v_N = power_iter(matrix(QQ, m_N))[1]
    
    SN_IRREPS = Partitions(N, max_length = d).list()
    
    for i, alpha in enumerate(Partitions(N-1, max_length = d).list()):
        for mu in add_box(alpha, d):
            res += v_Nm1[i] * v_N[SN_IRREPS.index(mu)] * sum_md_sq(alpha, d) / mul(alpha, d)**(1/2) / m.sqrt(N * dim(alpha) - dt(alpha, d)) / d**(3/2)
    return res

if  __name__ == "__main__":
    print(26, F_opt(26, 2), F_opt(26, 4), F_opt(26, 8), sep=',')
