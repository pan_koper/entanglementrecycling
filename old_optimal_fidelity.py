import math
import sys


import math as m
import scipy.special as ssp


def mj(j):
    return 2*j+1


def dj(N, j):
    return mj(j) / ( (N+2*j)/ 2 +1) * ssp.comb(N, (N+2*j)/2, exact=True)


def ml(N, l):
    return N - 2*l +1


def dl(N, l):
    return (N - 2*l + 1)/(N + 1) * ssp.comb(N + 1, l, exact=True)


def v(N, j):

    if N%2:
        return (-1)**round(j-1/2) * m.sin((j+1/2)*N*m.pi / (N+2)) / m.sin(N*m.pi/(N+2))

    else:
        return (-1)**j * (m.sin((j+1)*N*m.pi / (N+2)) - m.sin(j*N*m.pi / (N+2))) / m.sin(N*m.pi/(N+2))


def v_norm(N, j):
    return v(N, j) / m.sqrt(sum([v(N, j)**2 if not N%2 else v(N, j+1/2)**2 for j in range( N//2+1)]))

def old_v_norm(N, j):
    return v(N, j) / m.sqrt(sum([v(N, _2j/2)**2 for _2j in range(N%2, N+2, 2)]))


def v_norm_l(N, l):

    j = (N - 2*l)/2
    return v_norm(N, j)


def dtl(N, l):
    return N*(N-l)*l * dl(N-1, l) / ((N+1-l)*(l+1)) 


def sum_md(N):
    res = 0
    for _2q in range (N%2, N, 2):
        res += m.sqrt(mj(_2q/2) * dj(N, _2q/2))
    return res

def sum_md_alpha_l(N, l):
    res = 0
    res += m.sqrt(ml(N+1, l)*dl(N+1,l))
    res += m.sqrt(ml(N+1, l+1)*dl(N+1,l+1)) if l <= (N-1)/2 else 0

    return res

def d_a_d_t(N, l):
    return m.sqrt(N*dl(N-1, l) - dtl(N, l))


def d_a_d_t2(N, l):
    return m.sqrt(dl(N-1, l) * (N+1)*N/((N+1-l)*(l+1)))


def F(N):
    res = 0
    for l in range(0, (N-1)//2+1, 1):
        res += v_norm_l(N-1, l) * v_norm_l(N, l) / ml(N-1, l)**(3/2) * sum_md(N) / d_a_d_t2(N, l) / m.sqrt(N)
        if l+1 <= N-1 - l: res += v_norm_l(N-1, l) * v_norm_l(N, l+1) / ml(N-1, l)**(3/2) * sum_md(N) / d_a_d_t2(N, l) / m.sqrt(N)
    res *= m.sqrt(N) / (4 * m.sqrt(2))

    return(res)

def F_alt(N):
    '''version where \mu \in \alpha, the correct one!'''
    res = 0
    for l in range(0, (N-1)//2+1, 1):
        res += v_norm_l(N-1, l) * v_norm_l(N, l) / ml(N-1, l)**(1/2) * sum_md_alpha_l(N-1, l) / d_a_d_t2(N, l) / m.sqrt(N)
        if l+1 <= N-1 - l: res += v_norm_l(N-1, l) * v_norm_l(N, l+1)  / ml(N-1, l)**(1/2) * sum_md_alpha_l(N-1, l) / d_a_d_t2(N, l) / m.sqrt(N)
    res *= m.sqrt(N) / (4 * m.sqrt(2))

    return(res)

if __name__ == "__main__":
    print("N", "F", sep=',', file=sys.stderr)
    for N in range (2, 100):
        print(N, F_alt(N), sep=',')
