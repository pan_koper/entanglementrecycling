import math as m
import scipy.special as ssp

''' in this script the expressions for tr pi and tr v sqr pi are merded into one sum'''



def F_ideal(N, d=2):
    '''formula basing on wrong assumptions, namely calculated on the restricted domain of POVM \Pi'''
    return d**(N+1)/(N * tr_pi(N))* N/pow(d, N+2) * m.sqrt(tr_pi(N, d) + 1 + 2/N) / m.sqrt(pow(d, N+1)) * tr_v_sqr_pi(N, d)

def F(N, d=2):
    return N/pow(d, N+2) * m.sqrt(tr_pi(N, d) + 1 + 2/N) / m.sqrt(pow(d, N+1)) * tr_v_sqr_pi(N, d)

def old_F(N, d=2):
    '''wrong formula derived in Generalized teleprotation.... (2011)'''
    return N/pow(d, N+2) * m.sqrt(tr_pi(N, d) + 1 + 2/N) / m.sqrt(pow(d, N-1)) * tr_v_sqr_pi(N, d)


def tr_pi(N, d=2):
    if d==2:
        return tr_pi_2(N) 
def tr_pi_2(N):
    res = 0
    k = m.ceil(N/2 -1)
    res += sum(
            [(N+1) * ssp.comb(N, l, exact=True) * (N-2*l)**2 / ((N + 1 -l)*(l + 1)) for l in range(k+1)]
            )
    res /= N

    return res

def tr_v_sqr_pi(N, d=2):
    if d==2:
        return tr_v_sqr_pi2(N)

def tr_v_sqr_pi2(N):
    res = 0
    k = m.ceil(N/2 -1)
    res += sum(
            [m.sqrt((N+1-l)*(l+1)/(N+1)) * (
                (N+1-2*l) * m.sqrt(1/(N+1)*ssp.comb(N+1, l, exact=True)) +\
                        (N+1-2*(l+1))*m.sqrt(1/(N+1)*ssp.comb(N+1, l+1, exact=True))
                )**2 for l in range(k+1)]
            )
    
    res /= N

    return res

def F_fin_q(N):
    '''simpliest expression for fidelity of resource state
    in non-optimal recycling scheme, employedy in th preprint
    arXiv:2105.14886v1, precisely in the frmula B30'''
    return m.sqrt(N)/2**(N+2) * tr_v_sqr_pi2(N)

def corr_F_fin_q(N):
    '''CORRECTED
    simpliest expression for fidelity of resource state
    in non-optimal recycling scheme, employedy in th preprint
    arXiv:2105.14886v1, precisely in the frmula B30'''
    return 2 * m.sqrt(N)/2**(N+2) * tr_v_sqr_pi2(N)

def F_bound_k(N, k):
    '''the lower bound on the fidelity of resource state after k rounds of
    recycling procedure, adopted from Generalized teleprotation.... (2011)
    to be included in the manuscript
    '''
    return 1 - 2 * k * (1 - corr_F_fin_q(N))

print('N', 'k=2', 'k=4', 'k=8', sep=',')

for N in range(3, 100):
    print(N, F_bound_k(N, 2), F_bound_k(N, 4), F_bound_k(N, 8), sep=',')
